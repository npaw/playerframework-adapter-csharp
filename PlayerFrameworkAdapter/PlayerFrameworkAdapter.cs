﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Youbora.adapter;
using Youbora.log;
using Microsoft.PlayerFramework;
using Microsoft.PlayerFramework.Adaptive;

namespace Youbora.PlayerFrameworkadapter
{
    public class PlayerFrameworkAdapter : Adapter
    {
        #region constructor

        public PlayerFrameworkAdapter(object playerref) : base(playerref) { }

        public override void RegisterListeners()
        {
            // Start Buffer Monitor
            Monitorplayhead(true, true);

            // Register events
            ((MediaPlayer)this.player).MediaEnded += Player_MediaEnded;
            ((MediaPlayer)this.player).MediaFailed += Player_MediaFailed;
            ((MediaPlayer)this.player).AdvertisingStateChanged += Player_AdvertisingStateChanged;
            ((MediaPlayer)this.player).CurrentStateChanged += Player_CurrentStateChanged;
            ((MediaPlayer)this.player).MediaClosed += Player_MediaClosed;
        }

        public override void UnregisterListeners()
        {
            this.monitor.Stop();
            // UnRegister events
            if (this.player != null)
            {
                ((MediaPlayer)this.player).MediaEnded -= Player_MediaEnded;
                ((MediaPlayer)this.player).MediaFailed -= Player_MediaFailed;
                ((MediaPlayer)this.player).AdvertisingStateChanged -= Player_AdvertisingStateChanged;
                ((MediaPlayer)this.player).CurrentStateChanged -= Player_CurrentStateChanged;
                ((MediaPlayer)this.player).MediaClosed -= Player_MediaClosed;
            }
        }
        #endregion
        #region eventHandlers
        private void Player_MediaClosed(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            FireStop();
        }

        private void Player_CurrentStateChanged(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            Log.Debug("Current State: " + ((MediaPlayer)this.player).CurrentState);

            if (((MediaPlayer)this.player).CurrentState == Windows.UI.Xaml.Media.MediaElementState.Paused)
            {
                FirePause();
            }
            else if (((MediaPlayer)this.player).CurrentState == Windows.UI.Xaml.Media.MediaElementState.Playing)
            {
                FireJoin();
                FireResume();
            }
            else if (((MediaPlayer)this.player).CurrentState == Windows.UI.Xaml.Media.MediaElementState.Opening)
            {
                FireStart();
            }
        }

        private void Player_AdvertisingStateChanged(object sender, Windows.UI.Xaml.RoutedPropertyChangedEventArgs<AdvertisingState> e)
        {
            Log.Debug("Ad State: " + e.OldValue + " > " + e.NewValue);
            if (e.NewValue == AdvertisingState.Linear || e.NewValue == AdvertisingState.NonLinear)
            {
                FirePause();
            }
            else if (e.NewValue == AdvertisingState.None)
            {
                FireResume();
            }
        }

        private void Player_MediaFailed(object sender, Windows.UI.Xaml.ExceptionRoutedEventArgs e)
        {
            FireError(e.ErrorMessage);
            FireStop();
        }

        private void Player_MediaEnded(object sender, MediaPlayerActionEventArgs e)
        {
            FireStop();
        }

        #endregion
        #region getters
        public override string GetVersion()
        {
            return "6.0.0" + "-PlayerFramework-adapter-csharp";
        }

        public override double GetPlayhead()
        {
            if (((MediaPlayer)player).IsLive)
            {
                return ((MediaPlayer)player).LivePosition.Value.TotalSeconds;
            }
            return ((MediaPlayer)player).Position.TotalSeconds;
        }

        public override double GetPlayrate()
        {
            if (flags.isPaused) return 0;
            return ((MediaPlayer)player).PlaybackRate;
        }

        public override double? GetDuration()
        {
            return ((MediaPlayer)player).Duration.TotalSeconds;
        }

        public override string GetResource()
        {
            return ((MediaPlayer)player).Source?.AbsoluteUri;
        }

        public override double GetBitrate()
        {
            foreach (var plugin in ((MediaPlayer)player).Plugins.OfType<AdaptivePlugin>())
            {
                if (plugin.Manager.CurrentBitrate != 0) {
                    return plugin.Manager.CurrentBitrate;
                }
            }
            return -1;
        }

        public override string GetRendition()
        {
            return ((MediaPlayer)player).MediaQuality.ToString();
        }

        public override bool? GetIsLive()
        {
            return ((MediaPlayer)player).IsLive;
        }

        public override string GetPlayerName()
        {
            return "PlayerFramework";
        }
        #endregion
    }
}
